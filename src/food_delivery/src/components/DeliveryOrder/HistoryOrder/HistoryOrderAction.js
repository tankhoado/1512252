import axios from 'axios'
export const UPDATE_HISTORY = 'UPDATE_HISTORY';
export const CLEAR_HISTORY = 'CLEAR_HISTORY';
export const LOADED = 'LOADED'

export const loadHistory = (token) => (dispatch) => {
    dispatch({
        type: CLEAR_HISTORY,
    });
    (async () => {
        try {
            const resp = await axios.get('https://food-delivery-server.herokuapp.com/order/getAll',
                {
                    headers: { "Authorization": 'Bearer ' + token }
                });
            if (resp.status === 200) {
                dispatch({ type: LOADED });
                resp.data.forEach(element => {
                    if (element.idRestaurant)
                        axios.get('https://food-delivery-server.herokuapp.com/restaurant/getMenu/' + element.idRestaurant).then(resp => {
                            element.nameRestaurant = resp.data.restaurant.name;
                            dispatch({
                                type: UPDATE_HISTORY,
                                data: element,
                            })
                        }).catch(e => console.log(e));
                });
            }
        } catch (e) {
            console.log(e.response);
        }
    })();
}