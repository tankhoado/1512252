import React, { Component } from 'react';
import { StyleSheet, Text, FlatList, View, Image, Button, ScrollView } from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';
import styles from '../../../../style';
import PropTypes from 'prop-types';
import { strings } from '../../../../localization/i18n';
class HistoryDetail extends Component {
    constructor(props) {
        super(props);
        this.state = undefined;
    }
    componentDidMount() {
        const { token, id } = this.props;
        (async () => {
            try {
                const resp = await axios.get('https://food-delivery-server.herokuapp.com/order/getOrder/' + id,
                    {
                        headers: { "Authorization": 'Bearer ' + token }
                    });
                if (resp.status === 200) {
                    const restaurant = await axios.get('https://food-delivery-server.herokuapp.com/restaurant/getMenu/' + resp.data.order.idRestaurant,
                        {
                            headers: { "Authorization": 'Bearer ' + token }
                        });
                    if (restaurant.status === 200) {
                        this.setState({
                            order: resp.data,
                            restaurant: restaurant.data,
                        });
                    }
                }
            } catch (e) {
                console.log(e.response);
            }
        })();
    }
    render() {
        if (!this.state)
            return (<View style={styles.container}>
                <Image style={{ width: 120, height: 120 }} source={{ uri: 'https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif' }} />
            </View>);
        const { restaurant, order } = this.state;
        return (
            <ScrollView style={localStyles.container}>
                <View style={localStyles.viewFare}>
                    <Text style={localStyles.fare}>{strings('total')}:</Text>
                    <Text style={[localStyles.fare, { fontWeight: '600' }]}> VND {order.order.totalPrice / 1000}K</Text>
                </View>
                <View style={localStyles.content}>
                    <Text>
                        <Image source={{ uri: 'https://i.imgur.com/uBBK5Da.png' }} style={{ width: 60, height: 60 }} />   {restaurant.restaurant.name}
                    </Text>
                    <Text style={{ marginTop: '3%' }}>
                        <Image source={{ uri: 'https://i.imgur.com/3HC5vl1.png' }} style={{ width: 60, height: 60 }} />   {order.order.address}
                    </Text>
                    <View style={{ marginTop: '10%' }}>
                        {order.details.map(item => {
                            restaurant.menu.forEach(element => {
                                if (element.id === item.idFood) {
                                    item.price = element.price;
                                    item.name = element.name;
                                }
                            })
                            return (
                                <View key={item.id}>
                                    <View style={styles.basketitem}>
                                        <Text style={{ flex: 1, paddingRight: 10 }}>{item.quantity}</Text>
                                        <Text style={{ flex: 10, textAlign: 'left', paddingLeft: 20 }}>{item.name}</Text>
                                        <Text style={{ flex: 3, textAlign: 'right' }}>{(item.price * item.quantity).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                                    </View>
                                    <View style={styles.lineStyle} />
                                </View>
                            )
                        })}
                    </View>
                    <View style={localStyles.last}>
                        <Text>{strings('orderFrom')}</Text>
                        <Text style={localStyles.fare}>{restaurant.restaurant.name}</Text>
                    </View>
                    <View style={styles.lineStyle} />
                    <View style={localStyles.last}>
                        <Text>{strings('shipTo')}</Text>
                        <Text style={localStyles.fare}>{order.order.address}</Text>
                    </View>
                    <View style={styles.lineStyle} />
                    <View style={localStyles.last}>
                        <Text>{order.order.note ? order.order.note : strings('additional')}</Text>
                    </View>
                    <View style={styles.lineStyle} />
                    <Button title={strings('report')} onPress={() => alert(strings('comingSoon'))} />
                </View>
            </ScrollView >
        );
    }
}

HistoryDetail.propTypes = {
    token: PropTypes.string,
    id: PropTypes.number,
}

const mapStateToProps = (state) => ({
    token: state.authenticationReducer.accessToken,
})

export default connect(mapStateToProps, undefined)(HistoryDetail);

const localStyles = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },
    viewFare: {
        marginTop: '5%',
        marginLeft: '5%',
        height: '10%',
        flexDirection: 'row',
    },
    fare: {
        fontSize: 16,
        color: 'black',
    },
    content: {
        padding: '5%',
        paddingBottom: '10%',
        backgroundColor: '#F5FCFF',
        width: '100%',
    },
    last: {
        flexDirection: 'column',
    }
})