import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../../style';
import {
    View,
    Text,
} from 'react-native';

class CustomCallout extends React.Component {
    render() {
        const { info } = this.props;
        return (
            <View style={styles.container}>
                <View>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{info.name}</Text>
                    <Text>{info.street}</Text>
                </View>
            </View>
        );
    }
}

CustomCallout.propTypes = {
    info: PropTypes.object,
};

export default CustomCallout;