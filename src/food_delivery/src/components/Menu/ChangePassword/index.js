import React, { Component } from 'react';
import { View, TextInput, Button, Alert } from 'react-native';
import styles from '../../../../style';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { strings } from '../../../../localization/i18n';
import { changePassword } from './ChangePasswordAction';
class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldPass: '',
            newPass: '',
            confirmNewPass: '',
        }
    }
    update() {
        const { token, changePassword, passHash } = this.props;
        if (this.state.oldPass == '') {
            Alert.alert(strings('error'), strings('passEmpty'));
            return;
        }
        if (this.state.newPass == '') {
            Alert.alert(strings('error'), strings('newPassEmpty'));
            return;
        }
        if (this.state.confirmNewPass == '') {
            Alert.alert(strings('error'), strings('confirmNewPassEmpty'));
            return;
        }
        if (this.state.newPass !== this.state.confirmNewPass)
            Alert.alert(strings('error'), strings('notCorrect'));
        else
            changePassword({
                token: token,
                passHash: passHash,
                oldPass: this.state.oldPass,
                newPass: this.state.newPass,
            });
    }
    render() {
        const { updating } = this.props;
        return (
            <View style={styles.container}>
                <TextInput style={{ width: '80%', fontSize: 18 }} secureTextEntry={true} placeholder={strings('oldPass')} onChangeText={(oldPass) => this.setState({ oldPass: oldPass })} />
                <TextInput style={{ width: '80%', fontSize: 18 }} secureTextEntry={true} placeholder={strings('newPass')} onChangeText={(newPass) => this.setState({ newPass: newPass })} />
                <TextInput style={{ width: '80%', fontSize: 18 }} secureTextEntry={true} placeholder={strings('confirm')} onChangeText={(confirmNewPass) => this.setState({ confirmNewPass: confirmNewPass })} />
                <View style={{ width: '80%' }} >
                    <Button disabled={updating} title={strings('update')} onPress={() => this.update()}></Button>
                </View>
            </View>
        );
    }
}

ChangePassword.propTypes = {
    token: PropTypes.string,
    updating: PropTypes.bool,
    passHash: PropTypes.string,
    changePassword: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    token: state.authenticationReducer.accessToken,
    passHash: state.authenticationReducer.passHash,
    updating: state.changePasswordReducer.updating
})

const mapDispatchToProps = (dispatch) => ({
    changePassword: (data) => { dispatch(changePassword(data)) }
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);