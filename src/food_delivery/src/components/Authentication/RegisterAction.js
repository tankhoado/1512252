import { Alert } from 'react-native';
import { strings } from '../../../localization/i18n';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
export const register = (email, password) => {
    if (email == '') {
        Alert.alert(strings('error'), strings('emailEmpty'));
        return;
    }
    if (password == '') {
        Alert.alert(strings('error'), strings('passEmpty'));
        return;
    }
    (async () => {
        try {
            const resp = await axios.post('https://food-delivery-server.herokuapp.com/register', {
                email: email,
                password: password
            });
            if (resp.status === 200) {
                Alert.alert(strings('success'), strings('register.success'), [{
                    text: 'OK', onPress: () => {
                        Actions.pop();
                    }
                }])
            }
        } catch (e) {
            if (e.response.status === 400) {
                Alert.alert(strings('error'), strings('register.emailExist'));
            }
            if (e.response.status === 401) {
                Alert.alert(strings('error'), strings('register.emailInvalid'));
            }
        }
    })();
}