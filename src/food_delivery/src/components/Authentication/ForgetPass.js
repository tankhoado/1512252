import React, { Component } from 'react';
import { View, TextInput, Button } from 'react-native';
import styles from '../../../style';
import { strings } from '../../../localization/i18n';
import { forgetPass } from './ForgetPassAction';
class ForgetPass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput style={{ width: '70%', fontSize: 18 }} keyboardType="email-address" placeholder="Email" onChangeText={(email) => this.setState({ email: email })} />
        <View style={{ width: '70%' }}>
          <Button title={strings('sendVerify')} onPress={() => forgetPass(this.state.email)}></Button>
        </View>
      </View>
    );
  }
}

export default ForgetPass;