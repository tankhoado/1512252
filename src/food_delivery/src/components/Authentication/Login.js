import React, { Component } from 'react';
import { Alert, View, TextInput, Button, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../../../style';
import axios from 'axios';
import { connect } from 'react-redux';
import { authenticate } from './AuthenticationAction';
import PropTypes from 'prop-types';
import { strings } from '../../../localization/i18n';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    }
  }
  componentWillReceiveProps(props) {
    const { isAuthenticated, error } = props;
    if (!isAuthenticated) {
      if (error === 400) {
        Alert.alert(strings('error'), strings('login.wrongInfo'));
      }
      if (error === 401) {
        Alert.alert(strings('error'), strings('login.notVerify'), [{
          text: strings('sendVerify'), onPress: () => {
            (async () => {
              try {
                const resp = await this.verifyAccount();
                if (resp.status === 200) {
                  Alert.alert(strings('success'), strings('login.sendEmail') + ': ' + this.state.email, [{ text: strings('confirm'), onPress: () => Actions.pop() }]);
                }
              } catch (e) {
                if (e.response.status === 400)
                  Alert.alert(strings('error'), e.response.data.msg);
              }
            })();
          }
        }, { text: strings('cancel') }]);
      }
    }
  }
  async verifyAccount() {
    const resp = await axios.post('https://food-delivery-server.herokuapp.com/verify', {
      email: this.state.email
    });
    return await resp;
  }
  login() {
    const { authenticate } = this.props;
    if (this.state.email == '') {
      Alert.alert(strings('error'), strings('emailEmpty'));
      return;
    }
    if (this.state.password == '') {
      Alert.alert(strings('error'), strings('passEmpty'));
      return;
    }
    authenticate({
      email: this.state.email,
      password: this.state.password
    })
  }
  render() {
    const { isAuthenticating } = this.props;
    return (
      <View style={styles.container}>
        <TextInput style={{ width: '70%', fontSize: 18 }} keyboardType="email-address" placeholder="Email" onChangeText={(email) => this.setState({ email: email })} />
        <TextInput style={{ width: '70%', fontSize: 18, paddingBottom: 20, marginBottom: 20 }} secureTextEntry={true} placeholder={strings('password')} onChangeText={(password) => this.setState({ password: password })} />
        <View style={{ width: '70%' }} >
          <Button disabled={isAuthenticating} title={strings('login.title')} onPress={() => this.login()}></Button>
        </View>
        <View style={styles.lineStyle} />
        <View style={{ width: '70%' }} >
          <Button disabled={isAuthenticating} title={strings('forgetPassword')} onPress={() => { Actions.forgetpass({ title: strings('forgetPassword') }) }}></Button>
        </View>
        <View style={styles.lineStyle} />
        <View style={{ width: '70%', flexDirection: 'row', justifyContent: 'center' }}>
          <Text style={{ fontSize: 18 }}>{strings('login.notMember')} </Text>
          <TouchableOpacity onPress={() => { Actions.register({ title: strings('register.title') }) }}>
            <Text style={{ fontSize: 18, color: 'blue', textDecorationLine: 'underline' }}>{strings('register.title')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

Login.propTypes = {
  isAuthenticating: PropTypes.bool,
  isAuthenticated: PropTypes.bool,
  error: PropTypes.number,
  accessToken: PropTypes.string,
  authenticate: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  isAuthenticating: state.authenticationReducer.isAuthenticating,
  isAuthenticated: state.authenticationReducer.isAuthenticated,
  accessToken: state.authenticationReducer.accessToken,
  error: state.authenticationReducer.error,
})

const mapDispatchToProps = (dispatch) => ({
  authenticate: (data) => {
    dispatch(authenticate(data));
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);