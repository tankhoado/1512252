import React, { Component } from 'react';
import { Text, View, SectionList, Button, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../../../../style';
import { connect } from 'react-redux';
import { loadMenu, addBasket, removeBasket } from './MerchantDetailAction';
import PropTypes from 'prop-types';
import { strings } from '../../../../localization/i18n';
import YouTube from 'react-native-youtube';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
class MerchantDetail extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const { loadMenu, id } = this.props;
        loadMenu(id);
    }
    render() {
        const { resource, loadingMenu, addBasket, menu, totalPrice, basketitem, id, merchant, removeBasket } = this.props;
        const overrideRender = ({ item, index, section }) => {
            const find = basketitem.find(obj => obj.id === item.id);
            return (
                <View style={{ backgroundColor: '#F5FCFF' }}>
                    <View key={index} style={styles.merchantdetailitem} >
                        <Image style={{ width: 100, height: 100, paddingLeft: 15 }} source={{ uri: item.image }} />
                        <Text style={{
                            flex: 2, paddingLeft: 22, fontSize: 16,
                            fontWeight: 'bold',
                        }}>{item.name}</Text>
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                            <Text style={{
                                flex: 1, textAlign: 'left', fontSize: 17,
                                fontWeight: 'bold',
                            }}>{item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                            {find ?
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => removeBasket(item)}>
                                        <Icon name='minus-circle-outline' size={35} color='#1B67FF' />
                                    </TouchableOpacity>
                                    <Text style={{ marginHorizontal: '15%', textAlign: 'center', fontSize: 17 }}>{find.amount}</Text>
                                    <TouchableOpacity onPress={() => addBasket(item)}>
                                        <Icon name='plus-circle-outline' size={35} color='#1B67FF' />
                                    </TouchableOpacity>
                                </View>
                                :
                                <TouchableOpacity onPress={() => addBasket(item)}>
                                    <Icon name='plus-circle-outline' size={35} color='#1B67FF' />
                                </TouchableOpacity>
                            }
                            {/* <Button title={strings('add')} onPress={() => addBasket(item)} /> */}
                        </View>
                    </View >
                </View>);
        };
        if (loadingMenu)
            return (
                <View style={styles.container}>
                    <Text style={{ fontSize: 26, color: '#1B67FF' }}>{strings('loadingData')}</Text>
                    <Image style={{ width: 120, height: 120 }} source={{ uri: 'https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif' }} />
                </View>
            );
        return (
            <View style={{ flexDirection: 'column-reverse' }}>
                <View style={{
                    position: 'absolute',
                    width: '100%',
                    height: 80,
                    zIndex: 2,
                    backgroundColor: '#F5FCFF',
                }}>
                    {basketitem.length !== 0 ? (<View style={{
                        paddingBottom: 20, paddingRight: 15,
                        paddingLeft: 15,
                        paddingTop: 20,
                    }}><Button title={strings('viewBasket') + '            ' + totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')} onPress={() => Actions.basket({ title: strings('basket'), id: id, merchant: merchant })} /></View>) : null}
                </View>
                <ScrollView style={{ zIndex: 1, marginBottom: 80 }}>
                    <View style={{ height: 150 }}>
                        <Swiper height={150} showsButtons={true}>
                            {resource.map((item, index) => {
                                if (item.type === 'image')
                                    return (
                                        <View key={index} style={styles.slide1}>
                                            <Image style={{ flex: 1 }} source={{ uri: item.url }} />
                                        </View>
                                    );
                                else {
                                    const videoId = item.url.substring(item.url.lastIndexOf('/') + 1);
                                    return (
                                        <YouTube key={index} apiKey='AIzaSyCFPF-L4heEcJ0WoGCJsWvy6A5b-eAN1-k' videoId={videoId} style={{ height: 150 }} />
                                    );
                                }
                            })}
                        </Swiper>
                    </View>
                    <SectionList
                        renderItem={overrideRender}
                        renderSectionHeader={({ section: { title } }) => (
                            <Text style={{ fontSize: 19, fontWeight: 'bold', paddingBottom: 22, paddingLeft: 10, backgroundColor: '#F5FCFF' }}>{title}</Text>
                        )}
                        sections={
                            [
                                {
                                    data: [...menu]
                                },
                            ]}
                        keyExtractor={(item, index) => item + index}
                    /></ScrollView>
            </View>
        );
    }
}

MerchantDetail.propTypes = {
    menu: PropTypes.array,
    basketitem: PropTypes.array,
    resource: PropTypes.array,
    totalPrice: PropTypes.number,
    loadMenu: PropTypes.func.isRequired,
    addBasket: PropTypes.func.isRequired,
    removeBasket: PropTypes.func.isRequired,
    loadingMenu: PropTypes.bool,
}

const mapStateToProps = (state) => ({
    menu: state.merchantDetailReducer.menu,
    basketitem: state.merchantDetailReducer.basketitem,
    totalPrice: state.merchantDetailReducer.totalPrice,
    loadingMenu: state.merchantDetailReducer.loadingMenu,
    resource: state.merchantDetailReducer.resource,
})

const mapDispatchToProps = (dispatch) => ({
    addBasket: (item) => { dispatch(addBasket(item)) },
    removeBasket: (item) => {dispatch(removeBasket(item))},
    loadMenu: (id) => { dispatch(loadMenu(id)) }
})

export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetail);