import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../../../style';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { strings } from '../../../localization/i18n';
class Loading extends Component {
    componentWillReceiveProps(props) {
        const { isAuthenticated } = props;
        if (isAuthenticated) {
            Actions.merchant({ title: strings('merchant') });
        }
        else {
            Actions.login({ title: strings('login.title') });
        }
    }
    componentDidMount() {
        const { isAuthenticated } = this.props;
        if (isAuthenticated) {
            setTimeout(() => {
                Actions.merchant();
            }, 10);
        }
        else {
            setTimeout(() => {
                Actions.login({ title: strings('login.title') });
            }, 10);
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 26, color: '#1B67FF'}}>{strings('loadingData')}</Text>
                <Image style={{width: 120, height: 120}} source={{uri: 'https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif'}} />
            </View>
        );
    }
}

Loading.propTypes = {
    isAuthenticated: PropTypes.bool,
};

const mapStateToProps = (state) => ({
    isAuthenticated: state.authenticationReducer.isAuthenticated,
})

export default connect(mapStateToProps, undefined)(Loading);