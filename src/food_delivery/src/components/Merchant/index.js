import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, TextInput, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from '../../../style';
import { connect } from 'react-redux';
import { addMerchant, searchMerchant } from './MerchantAction';
import { clearMenu } from './MerchantDetail/MerchantDetailAction';
import SideMenu from 'react-native-side-menu';
import Menu from '../Menu';
import debounce from 'lodash/debounce';
import PropTypes from 'prop-types';
import DiaLogAddress from '../Dialog/Address';
import { strings } from '../../../localization/i18n';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/Feather';
const wallpaper = [
  'https://i.imgur.com/Duh6DnN.jpg',
  'https://i.imgur.com/Ia9zDZA.jpg',
  'https://i.imgur.com/LNHl4iK.jpg',
  'https://i.imgur.com/bogcZB2.jpg',
  'https://i.imgur.com/PpEmBH4.jpg',
  'https://i.imgur.com/6i8yqXd.jpg',
  'https://i.imgur.com/4nSqIs2.jpg',
];

class Merchant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isOpen: false
    }
  }
  componentDidMount() {
    const { addMerchant } = this.props;
    addMerchant(1);
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }
  onMenuItemSelected = () =>
    this.setState({
      isOpen: false,
    });
  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 10;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };
  searchMerchant(text) {
    const { addMerchant, searchMerchant } = this.props;
    if (text === '')
      addMerchant(1);
    else
      searchMerchant(text);
  }
  render() {
    const { loadingMerchant, ward, merchantList, addr, clearMenu, nextPage, addMerchant } = this.props;
    const showadd = addr === '' ? ward.name : addr + ', ' + ward.name;
    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)}
      >
        <View style={styles.merchant}>
          <View style={{ marginTop: '10%', flexDirection: 'row', alignItems: 'center' }}>
            <TouchableHighlight
              onPress={() => this.toggle()}
              style={styles.buttonMenu}
            >
              <Icon name='menu' size={30} color='#1B67FF' />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.setState({ visible: true })}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ fontSize: 16 }}>{strings('deliverTo')} </Text>
                <Image source={{ uri: 'https://i.imgur.com/3HC5vl1.png' }} style={{ width: 16, height: 16 }} />
                <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'black' }}> {showadd}</Text>
              </View>
            </TouchableHighlight>
          </View>
          <View style={{ flexDirection: 'row', marginHorizontal: '2%', alignItems: 'center' }}>
            <Icon name='search' size={30} color='black' style={{ marginTop: '1%' }} />
            <TextInput style={{ marginBottom: '3%', marginLeft: '2%', width: '100%', fontSize: 18 }} placeholder={strings('search')} onChangeText={debounce((text) => this.searchMerchant(text), 500)} />
          </View>
          <ScrollView onScroll={({ nativeEvent }) => {
            if (this.isCloseToBottom(nativeEvent) && nextPage !== -1 && !loadingMerchant)
              addMerchant(nextPage);
          }} scrollEventThrottle={400} >
            <View style={{ height: 150 }}>
              <Swiper height={150} showsButtons autoplay>
                {wallpaper.map((item, index) => <Image key={index} source={{ uri: item }} style={{ width: '100%', height: 150 }} />)}
              </Swiper>
            </View>
            <View>
              <Text style={{ fontWeight: 'bold', paddingBottom: 10, paddingLeft: 20, fontSize: 18 }}>{strings('deliveringYou')}</Text>
              {!loadingMerchant && merchantList.length === 0 ? <Text style={{ fontSize: 14, textAlign: 'center' }}>{strings('nomerchantfound')}</Text> :
                <FlatList data={merchantList}
                  renderItem={({ item }) => {
                    const info = item.info ? item.info : item;
                    let rating = [];
                    for (let i = 0; i < info.rating; i++)
                      rating.push(<Image key={i} source={{ uri: 'https://png.pngtree.com/svg/20170626/star_rating_425874.png' }} style={{ width: 24, height: 24 }} />);
                    return (
                      <TouchableHighlight onPress={() => { clearMenu(); Actions.merchantDetail({ title: info.name, id: info.id, merchant: info.name }); }}>
                        <View style={styles.merchantitem}>
                          <Image style={{ width: 100, height: 100 }} source={{ uri: info.image }} />
                          <View style={styles.merchantitemtext}>
                            <Text style={{ color: '#000000', fontSize: 18, fontWeight: 'bold' }}>{info.name}</Text>
                            <Text>{info.category}</Text>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                              {rating}
                            </View>
                          </View>
                        </View>
                      </TouchableHighlight>
                    )
                  }}
                  keyExtractor={(item, index) => index.toString()} />
              }
            </View>
            <DiaLogAddress visible={this.state.visible} hideDialog={() => this.setState({ visible: false })} />
            {loadingMerchant ?
              <View style={{ alignItems: 'center' }}>
                <Image style={{ width: 120, height: 120 }} source={{ uri: 'https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif' }} />
              </View> : undefined}
          </ScrollView>
        </View >
      </SideMenu>
    );
  }
}

Merchant.propTypes = {
  isAuthenticated: PropTypes.bool,
  addMerchant: PropTypes.func.isRequired,
  searchMerchant: PropTypes.func.isRequired,
  clearMenu: PropTypes.func.isRequired,
  addr: PropTypes.string,
  merchantList: PropTypes.array,
  nextPage: PropTypes.number,
  ward: PropTypes.object,
  loadingMerchant: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.authenticationReducer.isAuthenticated,
  addr: state.addressReducer.addr,
  nextPage: state.merchantReducer.nextPage,
  merchantList: state.merchantReducer.merchantList,
  ward: state.addressReducer.ward,
  loadingMerchant: state.merchantReducer.loadingMerchant,
})

const mapDispatchToProps = (dispatch) => ({
  addMerchant: (page) => { dispatch(addMerchant(page)) },
  clearMenu: () => { dispatch(clearMenu()) },
  searchMerchant: (text) => { dispatch(searchMerchant(text)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(Merchant);