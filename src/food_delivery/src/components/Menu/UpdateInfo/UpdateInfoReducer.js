import { UPDATE_REQUEST, UPDATE_SUCCESS, UPDATE_FAIL, UPDATE_AVATAR_SUCCESS } from './UpdateInfoAction';
const initialState = {
    updating: false,
    switchOn: true,
};

const updateInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_REQUEST:
            return { ...state, updating: true };
        case UPDATE_SUCCESS:
            return { ...state, updating: false, switchOn: action.switchOn };
        case UPDATE_FAIL:
            return { ...state, updating: false };
        case UPDATE_AVATAR_SUCCESS:
            return { ...state, updating: false };
        default:
            return state;
    }
}

export default updateInfoReducer;