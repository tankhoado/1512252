import React, { Component } from 'react';
import { View, TextInput, Button } from 'react-native';
import styles from '../../../style';
import { strings } from '../../../localization/i18n';
import { register } from './RegisterAction';
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput style={{ width: '70%', fontSize: 18 }} keyboardType="email-address" placeholder="Email"
          onChangeText={(email) =>
            this.setState({ email: email })} />
        <TextInput style={{ width: '70%', fontSize: 18, paddingBottom: 20 }} secureTextEntry={true} placeholder={strings('password')}
          onChangeText={(password) =>
            this.setState({
              password: password
            })} />
        <View style={{ width: '70%' }} >
          <Button title={strings('confirm')} onPress={() => register(this.state.email, this.state.password)}></Button>
        </View>
      </View>
    );
  }
}

export default Register;