export const UPDATE_OPTION = 'UPDATE_OPTION';

export const updateOption = (option) => ({
    type: UPDATE_OPTION,
    data: option,
})