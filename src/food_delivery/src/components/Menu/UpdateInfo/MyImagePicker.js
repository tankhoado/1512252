import React, { Component } from 'react';
import {
    View, StyleSheet,
    Image, TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ImagePicker from 'react-native-image-crop-picker';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
    },
});

export default class MyImagePicker extends Component {
    pickSingleWithCamera() {
        ImagePicker.openCamera({
            cropping: true,
            enableRotationGesture: true,
        }).then(image => {
            this.props.onChange(image.path);
        }).catch(e => { });
    }

    pickSingle() {
        ImagePicker.openPicker({
            cropping: true,
            enableRotationGesture: true,
            compressVideoPreset: 'MediumQuality',
            includeExif: true,
        }).then(image => {
            this.props.onChange(image.path);
        }).catch(e => { });
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={{ width: 170, height: 170 }} source={{ uri: this.props.defaultAvatar, cache: 'force-cache' }} />
                <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={() => this.pickSingleWithCamera()}>
                        <Icon name='camera' size={50} color='black' style={{ marginTop: '1%' }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.pickSingle()}>
                        <Icon name='image-album' size={50} color='black' style={{ marginTop: '1%' }} />
                    </TouchableOpacity>
                </View>
            </View>);
    }
}