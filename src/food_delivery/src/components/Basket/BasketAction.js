import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { strings } from '../../../localization/i18n';
export async function getInfo(token) {
    const resp = await axios.get('https://food-delivery-server.herokuapp.com/getinfo/v2', {
        headers: { "Authorization": 'Bearer ' + token }
    });
    return await resp;
}

async function postOrder(totalPrice, props, phone) {
    const { district, ward, addr, basketitem, token, id, option } = props;
    const resp = await axios.post('https://food-delivery-server.herokuapp.com/order/create/v2', {
        totalPrice,
        address: addr + ', ' + ward.name + ', ' + district.name,
        idRestaurant: id,
        phone,
        note: option,
        item: basketitem.map(i => {
            let ret = {
                idFood: i.id,
                quantity: i.amount,
            }
            return ret;
        })
    }, {
            headers: { "Authorization": 'Bearer ' + token }
        });
    return await resp;
}

export function createOrder(totalPrice, props, phone) {
    (async () => {
        try {
            const resp = await postOrder(totalPrice, props, phone);
            if (resp.status === 200) {
                Actions.deliveryOrder({ title: strings('deliveryOrder'), totalPrice: totalPrice, merchant: props.merchant });
            }
        } catch (e) {
            console.log(e);
        }
    })();
}