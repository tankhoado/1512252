import { DISCOUNT } from '../../Merchant/MerchantDetail/MerchantDetailAction';
export const UPDATE_PROMO = 'UPDATE_PROMO';

export const updatePromo = (promo) => (dispatch) => {
    dispatch({
        type: UPDATE_PROMO,
        data: promo,
    });
    if (promo.toUpperCase() === 'NEWYEAR')
        dispatch({
            type: DISCOUNT,
            data: 0.5,
        });
    else
        dispatch({
            type: DISCOUNT,
            data: 0,
        });
}