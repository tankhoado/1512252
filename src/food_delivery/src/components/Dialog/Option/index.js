import React, { Component } from 'react';
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
} from 'react-native-popup-dialog';
import { View, TextInput, Text } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { updateOption } from './OptionAction';
import { strings } from '../../../../localization/i18n';
class DialogOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            option: props.option,
        }
    }
    render() {
        const { option, visible, hideDialog, updateOption } = this.props;
        return (
            <Dialog width={0.9} visible={visible} dialogTitle={
                <DialogTitle
                    title={strings('option')}
                    hasTitleBar={true}
                    align="left"
                />} actions={[<DialogButton align='right' key='confirm' text={strings('confirm')} onPress={() => {
                    updateOption(this.state.option)
                    hideDialog();
                }
                } />, <DialogButton align='right' key='cancel' text={strings('cancel')} onPress={() => {
                    this.setState({ option: option });
                    hideDialog();
                }
                } />]}
            >
                <DialogContent>
                    <View>
                        <Text style={{ fontSize: 14 }}>{strings('noteDriver')}</Text>
                        <TextInput style={{ fontSize: 14 }} placeholder={strings('additional2')} value={this.state.option} onChangeText={(option) => this.setState({ option: option })} />
                    </View>
                </DialogContent>
            </Dialog>
        );
    }
}

DialogOption.propTypes = {
    visible: PropTypes.bool,
    option: PropTypes.string,
    updateOption: PropTypes.func.isRequired,
    hideDialog: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    option: state.optionReducer.option,
})

const mapDispatchToProps = (dispatch) => ({
    updateOption: (option) => { dispatch(updateOption(option)) }
})

export default connect(mapStateToProps, mapDispatchToProps)(DialogOption)