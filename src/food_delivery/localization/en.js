export const en = {
    login: {
        title: 'Login',
        notMember: 'Not a member?',
        wrongInfo: 'Email or Password is wrong',
        notVerify: 'Account is not verified',
        sendEmail: 'An email has been sent to',
    },
    merchant: 'Merchant',
    detail: 'Detai',
    forgetPassword: 'Forget Password',
    register: {
        title: 'Register',
        emailInvalid: 'Email is not valid',
        emailExist: 'Email is exist',
        success: 'Please verify your email',
    },
    emailEmpty: 'Email is required',
    passEmpty: 'Password is required',
    basket: 'Basket',
    deliveryOrder: 'Delivery Order',
    history: 'History',
    noOrder: 'Your history is empty.',
    updateInfo: 'Update Info',
    map: 'Map',
    list: 'List',
    password: 'Password',
    confirm: 'Confirm',
    error: 'Error',
    success: 'Success',
    cancel: 'Cancel',
    sendVerify: 'Send Verify',
    search: 'Search',
    deliveringYou: 'Delivering to You',
    deliverTo: 'Deliver to',
    signOut: 'Sign out',
    viewBasket: 'View basket',
    additional: 'Additional info, e.g. building name, floor, unit',
    additional2: 'E.g. building name, floor, unit',
    orderFrom: 'Your Order From',
    subTotal: 'Subtotal',
    deliveryFees: 'Delivery Fees',
    taxes: 'Taxes',
    total: 'Total',
    comingSoon: 'Coming Soon',
    cash: 'Cash',
    promo: 'Promo',
    option: 'Option',
    placeOrder: 'Place Order',
    noteDriver: 'Note to Driver',
    enterAddress: 'Enter address',
    address: 'Address',
    report: 'Report an issue',
    tagThis: 'Tag this trip as',
    personal: 'Personal',
    backHome: 'Back to home',
    phone: 'Phone',
    update: 'Update',
    changePass: 'Change Password',
    matchOldPass: 'Your new password must be different from your previous password.',
    oldPass: 'Old password',
    newPass: 'New password',
    newPassEmpty: 'New password is required',
    confirmNewPassEmpty: 'Confirm new password is required',
    notCorrect: 'Confirm new password is not correct',
    oldPassWrong: 'Old password is not correct',
    changePasswordSuccess: 'Change password successful',
    shipTo: 'Delivery Location',
    loadingData: 'Loading data...',
    nomerchantfound: 'Let\'s try a different name.',
    yourPromoCode: 'Enter your promo code',
}