import React, { Component } from 'react';
import { Picker, View, TextInput } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { updateDistrict, updateWard, updateAddr, getDistrict, getWard } from './AddressAction';
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
} from 'react-native-popup-dialog';
import { strings } from '../../../../localization/i18n';
class DialogAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listDistrict: [],
            listWard: [],
        }
    }
    componentDidMount() {
        const { updateDistrict, updateWard, district, ward } = this.props;
        (async () => {
            try {
                const listDistrict = await getDistrict();
                this.setState({ listDistrict: [...listDistrict] });
                if (district.id === -1) {
                    updateDistrict(listDistrict[0]);
                    const listWard = await getWard(listDistrict[0].id);
                    this.setState({ listWard: [...listWard] });
                    if (ward.id === -1)
                        updateWard(listWard[0]);
                } else {
                    const listWard = await getWard(district.id);
                    this.setState({ listWard: [...listWard] });
                }
            } catch (e) {
                console.log(e);
            }
        })();
    }
    reloadWard(id) {
        const { updateWard } = this.props;
        (async () => {
            try {
                const ward = await getWard(id);
                this.setState({ listWard: [...ward] });
                updateWard(ward[0]);
            } catch (e) {
                console.log(e);
            }
        })();
    }
    render() {
        const { loadingWard, visible, hideDialog, updateAddr, district, ward, updateDistrict, addr, updateWard } = this.props;
        const indexDistrict = this.state.listDistrict.findIndex(obj => obj.id === district.id);
        const indexWard = this.state.listWard.findIndex(obj => obj.id === ward.id);
        return (
            <Dialog width={0.9} visible={visible} dialogTitle={
                <DialogTitle
                    title={strings('enterAddress')}
                    hasTitleBar={true}
                    align="left"
                />} actions={[<DialogButton key='confirm' text={strings('confirm')} onPress={() => hideDialog()} />]} onTouchOutside={() => hideDialog()}
            >
                <DialogContent>
                    <View>
                        <View style={{ width: '100%' }}>
                            <TextInput placeholder={strings('address')} value={addr} onChangeText={(addr) => updateAddr(addr)}></TextInput>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                            <Picker
                                selectedValue={this.state.listDistrict[indexDistrict]}
                                style={{ height: 50, width: '50%' }}
                                onValueChange={(itemValue) => {
                                    updateDistrict(itemValue);
                                    this.reloadWard(itemValue.id);
                                }
                                }>
                                {this.state.listDistrict.map(item => {
                                    return (<Picker.Item key={item.id} label={item.name} value={item} />)
                                })}
                            </Picker>
                            <Picker enabled={!loadingWard}
                                selectedValue={this.state.listWard[indexWard]}
                                style={{ height: 50, width: '50%' }}
                                onValueChange={(itemValue) => updateWard(itemValue)}>
                                {this.state.listWard.map(item => {
                                    return (<Picker.Item key={item.id} label={item.name} value={item} />)
                                })}
                            </Picker>
                        </View>
                    </View>
                </DialogContent>
            </Dialog>
        );
    }
}

DialogAddress.propTypes = {
    addr: PropTypes.string,
    district: PropTypes.object,
    loadingWard: PropTypes.bool,
    ward: PropTypes.object,
    updateWard: PropTypes.func.isRequired,
    hideDialog: PropTypes.func.isRequired,
    updateDistrict: PropTypes.func.isRequired,
    updateAddr: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    loadingWard: state.addressReducer.loadingWard,
    addr: state.addressReducer.addr,
    district: state.addressReducer.district,
    merchantList: state.merchantReducer.merchantList,
    ward: state.addressReducer.ward
})

const mapDispatchToProps = (dispatch) => ({
    updateWard: (ward) => { dispatch(updateWard(ward)) },
    updateAddr: (addr) => { dispatch(updateAddr(addr)) },
    updateDistrict: (district) => { dispatch(updateDistrict(district)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(DialogAddress)