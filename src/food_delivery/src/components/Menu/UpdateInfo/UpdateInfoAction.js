import axios from 'axios';
export const UPDATE_REQUEST = 'UPDATE_REQUEST';
export const UPDATE_SUCCESS = 'UPDATE_SUCCESS';
export const UPDATE_FAIL = 'UPDATE_FAIL';
export const UPDATE_AVATAR_SUCCESS = 'UPDATE_AVATAR_SUCCESS';

export const updateInfo = (data) => (dispatch) => {
    dispatch({
        type: UPDATE_REQUEST
    });
    (async () => {
        try {
            const resp = await axios.post('https://food-delivery-server.herokuapp.com/updateInfo', {
                phone: data.phone,
            }, {
                    headers: { "Authorization": 'Bearer ' + data.token }
                });
            if (resp.status === 200) {
                dispatch({
                    type: UPDATE_SUCCESS,
                    switchOn: data.switchOn,
                });
            }
        } catch (e) {
            dispatch({
                type: UPDATE_FAIL
            });
            console.log(e.response);
        }
    })();
}

export const updateAvatar = (data) => (dispatch) => {
    dispatch({
        type: UPDATE_REQUEST
    });
    (async () => {
        try {
            let formdata = new FormData();
            formdata.append('file', {
                uri: data.uri,
                type: 'image/jpeg',
                name: 'teste'
            });
            const resp = await axios.put('https://food-delivery-server.herokuapp.com/updateAvatar/v2', formdata, {
                headers: { "Authorization": 'Bearer ' + data.token }
            });
            if (resp.status === 200) {
                dispatch({
                    type: UPDATE_AVATAR_SUCCESS,
                });
            }
        } catch (e) {
            dispatch({
                type: UPDATE_FAIL
            });
            console.log(e.response);
        }
    })();
}
