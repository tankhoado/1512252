import React, { Component } from 'react';
import { Text, FlatList, View, Image, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import styles from '../../../../style';
import { loadHistory } from './HistoryOrderAction';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { strings } from '../../../../localization/i18n';
import Icon from 'react-native-vector-icons/MaterialIcons';
class HistoryOrder extends Component {
    componentDidMount() {
        const { loadHistory, token } = this.props;
        loadHistory(token);
    }
    render() {
        const { historyOrder, loadingHistory } = this.props;
        if (loadingHistory)
            return (<View style={styles.container}>
                <Image style={{ width: 120, height: 120 }} source={{ uri: 'https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif' }} />
            </View>);
        if (historyOrder.length === 0)
            return (<View style={styles.container}>
                <Icon name='report-problem' size={50} />
                <Text style={{ fontSize: 20 }}>{strings('noOrder')}</Text>
            </View>);
        return (
            <FlatList data={historyOrder}
                renderItem={({ item }) => {
                    const date = new Date(item.date);
                    return (
                        <TouchableHighlight onPress={() => Actions.historyDetail({ title: date.toLocaleDateString('en-US') + ', ' + date.toLocaleTimeString('en-US'), id: item.id })}>
                            <View style={styles.historyContainer}>
                                <View>
                                    <Text style={{ marginTop: '2%', color: 'black', textAlign: 'right', fontWeight: 'bold' }}>{date.toLocaleDateString('en-US') + ', ' + date.toLocaleTimeString('en-US')}</Text>
                                </View>
                                <View style={styles.lineStyle} />
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginBottom: '2%', alignItems: 'center' }}>
                                    <View style={{ flex: 5 }}>
                                        <Text>
                                            <Image source={{ uri: 'https://i.imgur.com/uBBK5Da.png' }} style={{ width: 60, height: 60 }} />   {item.nameRestaurant}
                                        </Text>
                                        <Text style={{ marginTop: '3%' }}>
                                            <Image source={{ uri: 'https://i.imgur.com/3HC5vl1.png' }} style={{ width: 60, height: 60 }} />   {item.address}
                                        </Text>
                                    </View>
                                    <Text style={{ flex: 1, fontSize: 16, color: 'blue', textAlign: 'right' }}>{item.totalPrice / 1000 + 'K'}</Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    )
                }}
                keyExtractor={(item, index) => index.toString()} />
        );
    }
}

HistoryOrder.propTypes = {
    token: PropTypes.string,
    historyOrder: PropTypes.array,
    loadHistory: PropTypes.func.isRequired,
    loadingHistory: PropTypes.bool,
}

const mapStateToProps = (state) => ({
    token: state.authenticationReducer.accessToken,
    historyOrder: state.historyOrderReducer.historyOrder,
    loadingHistory: state.historyOrderReducer.loadingHistory,
})

const mapDispatchToProps = (dispatch) => ({
    loadHistory: (token) => { dispatch(loadHistory(token)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(HistoryOrder);