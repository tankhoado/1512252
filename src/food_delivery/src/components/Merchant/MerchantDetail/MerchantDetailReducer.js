import { CLEAR_MENU, SET_MENU, ADD_BASKET, LOADING_MENU, DISCOUNT, REMOVE_BASKET } from './MerchantDetailAction';

const initialState = {
    menu: [],
    resource: [],
    basketitem: [],
    discount: 0,
    totalPrice: 0,
    loadingMenu: false,
};

const maxDiscount = 100000;
const rate = 0.5

const merchantDetailReducer = (state = initialState, action) => {
    let find, newPrice;
    switch (action.type) {
        case LOADING_MENU:
            return { ...state, loadingMenu: true };
        case SET_MENU:
            return { ...state, menu: [...action.data.menu], resource: [...action.data.resource], loadingMenu: false };
        case ADD_BASKET:
            find = state.basketitem.find(obj => obj.id === action.data.id);
            newPrice = state.totalPrice + action.data.price;
            if (!find) {
                action.data.amount = 1;
                return { ...state, basketitem: state.basketitem.concat(action.data), totalPrice: newPrice, discount: state.discount !== 0 ? Math.min(maxDiscount, rate * newPrice) : 0 }
            } else {
                find.amount++;
                return { ...state, totalPrice: newPrice, discount: state.discount !== 0 ? Math.min(maxDiscount, rate * newPrice) : 0 };
            }
        case REMOVE_BASKET:
            find = state.basketitem.find(obj => obj.id === action.data.id);
            newPrice = state.totalPrice - action.data.price;
            if (find) {
                find.amount--;
                if (find.amount === 0)
                    return { ...state, basketitem: state.basketitem.filter(val => val.id !== action.data.id), totalPrice: newPrice, discount: state.discount !== 0 ? Math.min(maxDiscount, rate * newPrice) : 0 };
                else
                    return { ...state, totalPrice: newPrice, discount: state.discount !== 0 ? Math.min(maxDiscount, rate * newPrice) : 0 };
            }
            return { ...state };
        case CLEAR_MENU:
            return { ...state, menu: [], basketitem: [], totalPrice: 0 };
        case DISCOUNT:
            return { ...state, discount: Math.min(maxDiscount, action.data * state.totalPrice) };
        default:
            return state;
    }
}

export default merchantDetailReducer;