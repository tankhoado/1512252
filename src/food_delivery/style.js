import { StyleSheet, Dimensions } from 'react-native'
const window = Dimensions.get('window');

export default StyleSheet.create({
  merchant: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  merchantitem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 22,
    paddingBottom: 25,
  },
  merchantitemtext: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingLeft: 20,
    paddingRight: 10,
  },
  merchantdetailitem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    paddingBottom: 20,
    paddingRight: 15,
    paddingLeft: 15,
  },
  basket: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#F5FCFF',
  },
  basketitem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    fontSize: 20,
    textAlign: 'center',
    padding: 10,
    color: '#000000',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'left',
    color: '#A4A4A4',
    paddingBottom: 5,
    paddingTop: 5,
    fontSize: 14,
  },
  info: {
    textAlign: 'left',
    color: '#000000',
    paddingBottom: 5,
    fontSize: 14,
  },
  lineStyle: {
    borderBottomWidth: 1,
    borderColor: '#848484',
    marginTop: 10,
    marginBottom: 10,
  },
  buttonMenu: {
    marginHorizontal: '2%',
    // top: 20,
    // padding: 10,
    // paddingBottom: '7%'
  },
  mapMenu: {
    top: 20,
    padding: 10,
    paddingBottom: '7%'
  },
  menuBar: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: 'gray',
    padding: 20,
  },
  menuItem: {
    fontSize: 16,
    fontWeight: '300',
    paddingTop: 5,
    color: 'white'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  mapContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  historyContainer: {
    flex: 1,
    paddingLeft: '2%',
    paddingRight: '2%',
    marginTop: 5,
    backgroundColor: '#F5FCFF',
    borderWidth: 1,
    borderColor: 'gray',
  },
  slide1: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});