import React, { Component } from 'react';
import { View, TextInput, Button, Switch, Text, Image, TouchableOpacity } from 'react-native';
import styles from '../../../../style';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { updateInfo, updateAvatar } from './UpdateInfoAction';
import { getInfo } from '../../Basket/BasketAction';
import MyImagePicker from './MyImagePicker';
class UpdateInfo extends Component {
    constructor(props) {
        super(props);
        const { switchOn } = this.props;
        this.state = {
            phone: '',
            switchOn: switchOn,
            avatar: '',
        }
    }
    componentDidMount() {
        const { token } = this.props;
        return getInfo(token).then(resp => {
            this.setState({
                phone: resp.data.phone,
                avatar: resp.data.avatarUrl,
            });
        }).catch(e => console.log(e.response));
    }
    update() {
        const { token, updateInfo, switchLanguage, updateAvatar } = this.props;
        updateInfo({
            token: token,
            phone: this.state.phone,
            switchOn: this.state.switchOn,
        });
        if (this.state.avatar.includes('file://'))
            updateAvatar({
                token: token,
                uri: this.state.avatar
            });
        switchLanguage(this.state.switchOn);
        Actions.pop();
    }
    render() {
        const { updating } = this.props;
        if (this.state.avatar === '')
            return (
                <View style={styles.container}>
                    <Image style={{ width: 120, height: 120 }} source={{ uri: 'https://loading.io/spinners/typing/lg.-text-entering-comment-loader.gif' }} />
                </View>
            )
        return (
            <View style={styles.container}>
                <MyImagePicker defaultAvatar={this.state.avatar} onChange={(uri) => this.setState({ avatar: uri })} />
                <TextInput style={{ width: '70%', fontSize: 18 }} keyboardType="phone-pad" placeholder={this.state.switchOn ? 'Số điện thoại' : 'Phone'} onChangeText={(phone) => this.setState({ phone: phone })} value={this.state.phone} />
                <View style={{ flexDirection: 'row', marginLeft: '15%', marginTop: '3%' }}>
                    <Switch value={this.state.switchOn} onValueChange={value => this.setState({ switchOn: value })} />
                    <Text style={{ flex: 1, marginLeft: '5%', fontSize: 18, fontWeight: 'bold' }}>{this.state.switchOn ? 'Tiếng Việt' : 'English'}</Text>
                </View>
                <View style={{ width: '70%', marginTop: '3%' }} >
                    <Button disabled={updating} title={this.state.switchOn ? 'Cập nhật' : 'Update'} onPress={() => this.update()}></Button>
                </View>
            </View >
        );
    }
}

UpdateInfo.propTypes = {
    token: PropTypes.string,
    updating: PropTypes.bool,
    updateInfo: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    token: state.authenticationReducer.accessToken,
    updating: state.updateInfoReducer.updating,
    switchOn: state.updateInfoReducer.switchOn,
})

const mapDispatchToProps = (dispatch) => ({
    updateInfo: (data) => { dispatch(updateInfo(data)) },
    updateAvatar: (data) => { dispatch(updateAvatar(data)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(UpdateInfo);