import axios from 'axios';

export const CLEAR_MENU = 'CLEAR_MENU';
export const SET_MENU = 'SET_MENU';
export const ADD_BASKET = 'ADD_BASKET';
export const REMOVE_BASKET = 'REMOVE_BASKET';
export const LOADING_MENU = 'LOADING_MENU';
export const DISCOUNT = 'DISCOUNT';
export const clearMenu = () => ({
    type: CLEAR_MENU
})

export const loadMenu = (id) => (dispatch) => {
    dispatch({
        type: LOADING_MENU,
    });
    (async () => {
        try {
            const json = await axios.get('https://food-delivery-server.herokuapp.com/restaurant/getMenu/' + id);
            dispatch({
                type: SET_MENU,
                data: json.data,
            });
        } catch (e) {
            console.log(e);
        }
    })();    
}

export const addBasket = (item) => ({
    type: ADD_BASKET,
    data: item,
})

export const removeBasket = (item) => ({
    type: REMOVE_BASKET,
    data: item,
})