import {
    UPDATE_OPTION
} from './OptionAction';

const initialState = {
    option: ''
}

const optionReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_OPTION:
            return { ...state, option: action.data };
        default:
            return state;
    }
}

export default optionReducer;