import axios from 'axios';
export const UPDATE_DISTRICT = 'UPDATE_DISTRICT';
export const UPDATE_WARD = 'UPDATE_WARD';
export const UPDATE_ADDR = 'UPDATE_ADDR';

export async function getDistrict() {
    const json = await axios.get('https://food-delivery-server.herokuapp.com/district/getAll');
    return json.data;
}
export async function getWard(id) {
    const json = await axios.get('https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=' + id);
    return json.data;
}

export const updateDistrict = (district) => ({
    type: UPDATE_DISTRICT,
    data: district,
})

export const updateWard = (ward) => ({
    type: UPDATE_WARD,
    data: ward,
})

export const updateAddr = (addr) => ({
    type: UPDATE_ADDR,
    data: addr,
})
