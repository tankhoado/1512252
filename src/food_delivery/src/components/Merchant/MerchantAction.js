import axios from 'axios';
export const LOADING_MERCHANT = 'LOADING_MERCHANT';
export const LOAD_SUCCESS = 'LOAD_SUCCESS';
export const LOAD_FAILURE = 'LOAD_FAILURE';
export const CLEAR_MERCHANT = 'CLEAR_MERCHANT';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'SEARCH_FAILURE';

export const addMerchant = (page) => (dispatch) => {
    if (page === 1)
        dispatch({
            type: CLEAR_MERCHANT,
        });
    dispatch({
        type: LOADING_MERCHANT,
    });
    (async () => {
        try {
            const data = await axios.get('https://food-delivery-server.herokuapp.com/restaurant/getAll/5&' + page);
            dispatch({
                type: LOAD_SUCCESS,
                data: data.data,
            });
        } catch (e) {
            dispatch({
                type: LOAD_FAILURE,
                message: e,
            })
        }
    })();
}

export const searchMerchant = (text) => (dispatch) => {
    dispatch({
        type: LOADING_MERCHANT,
    });
    (async () => {
        try {
            const data = await axios.get('https://food-delivery-server.herokuapp.com/restaurant/search?name=' + text);
            dispatch({
                type: SEARCH_SUCCESS,
                data: data.data,
            });
        } catch (e) {
            dispatch({
                type: SEARCH_FAILURE,
                message: e,
            })
        }
    })();
}
