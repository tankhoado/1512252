import React, { Component } from 'react';
import { Text, View, FlatList, Button, Image } from 'react-native';
import styles from '../../../style';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { strings } from '../../../localization/i18n';
class DeliveryOrder extends Component {
  render() {
    const { district, ward, addr, merchant, basketitem, option } = this.props;
    return (
      <View style={{ padding: 15, backgroundColor: '#F5FCFF' }}>
        <Text style={{ paddingBottom: 20 }}>{strings('total')}: <Text style={{ color: 'black' }}>VND {this.props.totalPrice / 1000}K</Text></Text>
        <Text style={{ color: '#A4A4A4' }}>{strings('tagThis')}</Text>
        <Text>{strings('personal')}</Text>
        <View style={styles.lineStyle} />
        <Text style={{ paddingBottom: 20 }}><Image source={{ uri: 'https://i.imgur.com/uBBK5Da.png' }} style={{ width: 60, height: 60 }} />  {merchant}</Text>
        <Text style={{ paddingBottom: 30 }}><Image source={{ uri: 'https://i.imgur.com/3HC5vl1.png' }} style={{ width: 60, height: 60 }} />  {addr + ', ' + ward.name + ', ' + district.name}</Text>
        <FlatList data={basketitem}
          renderItem={({ item }) => (
            <View>
              <View style={styles.basketitem}>
                <Text style={{ flex: 1, paddingRight: 10 }}>{item.amount}</Text>
                <Text style={{ flex: 10, textAlign: 'left', paddingLeft: 20 }}>{item.name}</Text>
                <Text style={{ flex: 3, textAlign: 'right' }}>{(item.price * item.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
              </View>
              <View style={styles.lineStyle} />
            </View>
          )}
          keyExtractor={(item, index) => index.toString()} />
        <Text style={styles.instructions}>{option === '' ? strings('additional') : option}</Text>
        <Button title={strings('report')} onPress={() => alert(strings('comingSoon'))} />
        <View style={{ margin: '2%' }} />
        <Button title={strings('backHome')} onPress={() => Actions.reset('loading')} />
      </View>
    );
  }
}
const mapStateToProps = (state) => ({
  basketitem: state.merchantDetailReducer.basketitem,
  addr: state.addressReducer.addr,
  district: state.addressReducer.district,
  ward: state.addressReducer.ward,
  option: state.optionReducer.option
})

export default connect(mapStateToProps, undefined)(DeliveryOrder);