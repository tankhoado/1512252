import { UPDATE_HISTORY, CLEAR_HISTORY, LOADED } from './HistoryOrderAction';

const initialState = {
    historyOrder: [],
    loadingHistory: false,
};

const historyOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_HISTORY:
            return { ...state, historyOrder: state.historyOrder.concat(action.data).sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()) };
        case CLEAR_HISTORY:
            return { ...state, loadingHistory: true, historyOrder: [] };
        case LOADED:
            return { ...state, loadingHistory: false }
        default:
            return state;
    }
}

export default historyOrderReducer;
