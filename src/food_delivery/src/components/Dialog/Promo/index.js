import React, { Component } from 'react';
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
} from 'react-native-popup-dialog';
import { View, TextInput } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { updatePromo } from './PromoAction';
import { strings } from '../../../../localization/i18n';
class DialogPromo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            promo: props.promo,
        }
    }
    render() {
        const { promo, visible, hideDialog, updatePromo } = this.props;
        return (
            <Dialog width={0.9} visible={visible} dialogTitle={
                <DialogTitle
                    title={strings('promo')}
                    hasTitleBar={true}
                    align="left"
                />} actions={[<DialogButton align='right' key='confirm' text={strings('confirm')} onPress={() => {
                    updatePromo(this.state.promo);
                    hideDialog();
                }
                } />, <DialogButton align='right' key='cancel' text={strings('cancel')} onPress={() => {
                    this.setState({ promo: promo });
                    hideDialog();
                }
                } />]} onTouchOutside={() => {
                    this.setState({ promo: promo })
                    hideDialog();
                }}
            >
                <DialogContent>
                    <View>
                        <TextInput style={{ fontSize: 14 }} placeholder={strings('yourPromoCode')} value={this.state.promo} onChangeText={(promo) => this.setState({ promo: promo })} />
                    </View>
                </DialogContent>
            </Dialog >
        );
    }
}

DialogPromo.propTypes = {
    visible: PropTypes.bool,
    hideDialog: PropTypes.func.isRequired,
    updatePromo: PropTypes.func.isRequired,
    promo: PropTypes.string,
};

const mapStateToProps = (state) => ({
    promo: state.promoReducer.promo
})

const mapDispatchToProps = (dispatch) => ({
    updatePromo: (promo) => { dispatch(updatePromo(promo)) },
})

export default connect(mapStateToProps, mapDispatchToProps)(DialogPromo)