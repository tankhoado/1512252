import React, { Component } from 'react';
import {
    View,
    Dimensions,
    StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import MapView, { ProviderPropType, Marker, Callout } from 'react-native-maps';
import CustomCallout from './CustomCallout';
import debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import { clearMenu } from '../Merchant/MerchantDetail/MerchantDetailAction';
import styles from '../../../style';
import { getNearMerchant } from './MapAction';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 10.768533;
const LONGITUDE = 106.701899;
const LATITUDE_DELTA = 0.0122;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            markers: [],
            myLocation: {
                latitude: 0,
                longitude: 0,
            }
        };
    }
    componentDidMount() {
        getNearMerchant(LATITUDE, LONGITUDE).then(data => {
            if (data)
                this.setState({
                    markers: data.map(merchant => {
                        let ret = {
                            key: merchant.id,
                            street: merchant.street,
                            name: merchant.RESTAURANT.name,
                            coordinate: {
                                latitude: merchant.latitude,
                                longitude: merchant.longitude,
                            }
                        }
                        return ret;
                    })
                });
        }).catch(e => console.log(e));
        navigator.geolocation.getCurrentPosition(position => {
            const lat = parseFloat(position.coords.latitude);
            const long = parseFloat(position.coords.longitude);
            this.setState({
                myLocation: {
                    latitude: lat,
                    longitude: long,
                }
            })
        }, error => console.log(JSON.stringify(error)))
    }
    reloadRegion(region) {
        getNearMerchant(region.latitude, region.longitude).then(data => {
            if (data)
                this.setState({
                    markers: data.map(merchant => {
                        let ret = {
                            key: merchant.id,
                            street: merchant.street,
                            name: merchant.RESTAURANT.name,
                            coordinate: {
                                latitude: merchant.latitude,
                                longitude: merchant.longitude,
                            }
                        }
                        return ret;
                    })
                })
        }).catch(e => console.log(e));
    }
    render() {
        const { clearMenu } = this.props;
        return (
            <View style={styles.mapContainer}>
                <MapView
                    provider={this.props.provider}
                    style={styles.map}
                    initialRegion={this.state.region}
                    onRegionChange={debounce((region) => this.reloadRegion(region), 1000)}
                >
                    {this.state.markers.map(marker => (
                        <Marker
                            key={marker.key}
                            coordinate={marker.coordinate}
                        >
                            <Callout onPress={() => { clearMenu(); Actions.merchantDetail({ title: marker.name, id: marker.key, merchant: marker.name }); }}>
                                <CustomCallout info={marker} />
                            </Callout>
                        </Marker>
                    ))}
                    <Marker coordinate={this.state.myLocation} >
                        <View style={GPS.radius} >
                            <View style={GPS.pulse} />
                        </View>
                    </Marker>
                </MapView>
            </View>
        )
    }
}

Map.propTypes = {
    provider: ProviderPropType,
    clearMenu: PropTypes.func.isRequired,
}

const mapDispatchToProps = (dispatch) => ({
    clearMenu: () => { dispatch(clearMenu()) }
})

export default connect(undefined, mapDispatchToProps)(Map);

const GPS = StyleSheet.create({
    pulse: {
        width: 20,
        height: 20,
        borderColor: 'white',
        borderStyle: 'solid',
        borderWidth: 3,
        borderRadius: 60,
        backgroundColor: '#0080FF',
        zIndex: 10,
    },
    radius: {
        width: 60,
        height: 60,
        borderStyle: 'solid',
        borderRadius: 200,
        backgroundColor: 'rgba(0,128,255,0.2)',
        zIndex: 10,
        alignItems: 'center',
        justifyContent: 'center'
    }
})