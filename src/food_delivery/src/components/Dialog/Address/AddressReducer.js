import {
    UPDATE_DISTRICT,
    UPDATE_WARD,
    UPDATE_ADDR
} from './AddressAction';

const initialState = {
    loadingWard: false,
    district: {
        id: -1,
        name: '',
    },
    ward: {
        id: -1,
        name: '',
        idDistrict: 0,
    },
    addr: '72 Lê Thánh Tôn',
}

const addressReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_DISTRICT:
            return { ...state, district: action.data, loadingWard: true };
        case UPDATE_WARD:
            return { ...state, ward: action.data, loadingWard: false };
        case UPDATE_ADDR:
            return { ...state, addr: action.data };
        default:
            return state;
    }
}

export default addressReducer;