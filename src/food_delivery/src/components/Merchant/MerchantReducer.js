import {
    LOADING_MERCHANT, LOAD_SUCCESS,
    LOAD_FAILURE, CLEAR_MERCHANT,
    SEARCH_SUCCESS, SEARCH_FAILURE,
} from './MerchantAction';

const initialState = {
    merchantList: [],
    nextPage: -1,
    loadingMerchant: false,
};

const merchantReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLEAR_MERCHANT:
            return { ...state, merchantList: [] };
        case LOADING_MERCHANT:
            return { ...state, loadingMerchant: true };
        case LOAD_SUCCESS:
            return { ...state, loadingMerchant: false, merchantList: state.merchantList.concat(action.data.data), nextPage: action.data.next_page };
        case LOAD_FAILURE:
            return { ...state, loadingMerchant: false, merchantList: [], nextPage: -1 };
        case SEARCH_SUCCESS:
            return { ...state, loadingMerchant: false, merchantList: [...action.data], nextPage: -1 };
        case SEARCH_FAILURE:
            return { ...state, loadingMerchant: false, merchantList: [], nextPage: -1 };
        default:
            return state;
    }
}

export default merchantReducer;