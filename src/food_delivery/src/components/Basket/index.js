import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, Button, Image, TouchableHighlight, TextInput } from 'react-native';
import { connect } from 'react-redux';
import styles from '../../../style';
import PropTypes from 'prop-types';
import DialogOption from '../Dialog/Option';
import DialogPromo from '../Dialog/Promo';
import { strings } from '../../../localization/i18n';
import { getInfo, createOrder } from './BasketAction';
class Basket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleOption: false,
            visiblePromo: false,
            phone: ''
        }
    }
    componentDidMount() {
        const { token } = this.props;
        (async () => {
            try {
                const resp = await getInfo(token);
                if (resp.status === 200) {
                    this.setState({
                        phone: resp.data.phone
                    });
                }
            } catch (e) {
                console.log(e.response);
            }
        })();
    }
    render() {
        const { discount, district, ward, addr, option, basketitem, totalPrice, merchant } = this.props;
        let deliveryFees = 15000;
        let taxes = 0;
        const items = basketitem;
        return (
            <ScrollView style={styles.basket}>
                <Text style={styles.instructions}>{strings('deliverTo')}</Text>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#2ECCFA' }}>{addr + ', ' + ward.name + ', ' + district.name}</Text>
                <TouchableHighlight onPress={() => this.setState({ visibleOption: true })}><Text style={styles.instructions}>{option === '' ? strings('additional') : option}</Text></TouchableHighlight>
                <Text style={styles.info}>
                    {strings('orderFrom')} <Text style={{ fontWeight: 'bold' }}>{merchant}</Text>
                </Text>
                <FlatList data={items}
                    renderItem={({ item }) => (
                        <View>
                            <View style={styles.lineStyle} />
                            <View style={styles.basketitem}>
                                <Text style={{ paddingRight: 10 }}>{item.amount}x</Text>
                                <Image style={{ width: 60, height: 60 }} source={{ uri: item.image }} />
                                <Text style={{ flex: 3, textAlign: 'left', paddingLeft: 20 }}>{item.name}</Text>
                                <Text style={{ flex: 1, textAlign: 'right' }}>{item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                            </View>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()} />
                <View style={styles.lineStyle} />
                {discount > 0 ?
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={{ color: '#2ECCFA', flex: 1 }}>{strings('promo')}</Text>
                        <Text style={{ color: '#2ECCFA' }}>{(discount * -1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                    </View> : undefined}
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={{ flex: 1 }}>{strings('subTotal')}</Text>
                    <Text>{totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', paddingTop: 5 }}>
                    <Text style={{ flex: 1 }}>{strings('deliveryFees')}</Text>
                    <Text>{deliveryFees.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', paddingTop: 5 }}>
                    <Text style={{ flex: 1 }}>{strings('taxes')}</Text>
                    <Text>{taxes}</Text>
                </View>
                <View style={styles.lineStyle} />
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={{ flex: 1, fontWeight: 'bold', color: 'black', fontSize: 18 }}>{strings('total')}</Text>
                    <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 18 }}>{(totalPrice + taxes + deliveryFees - discount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</Text>
                </View>
                <View style={styles.lineStyle} />
                <View style={{ flex: 1, flexDirection: 'row', paddingTop: 20, paddingBottom: 30, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Button title={strings('cash')} onPress={() => alert(strings('comingSoon'))} />
                    <Button title={strings('promo')} onPress={() => this.setState({ visiblePromo: true })} />
                    <Button title={strings('option')} onPress={() => this.setState({ visibleOption: true })} />
                </View>
                <View>
                    <Button title={strings('placeOrder')} onPress={() => { createOrder(totalPrice + taxes + deliveryFees - discount, this.props, this.state.phone) }} />
                </View>
                <DialogOption visible={this.state.visibleOption} hideDialog={() => this.setState({ visibleOption: false })}></DialogOption>
                <DialogPromo visible={this.state.visiblePromo} hideDialog={() => this.setState({ visiblePromo: false })}></DialogPromo>
            </ScrollView>
        );
    }
}

Basket.propTypes = {
    token: PropTypes.string,
    basketitem: PropTypes.array,
    addr: PropTypes.string,
    district: PropTypes.object,
    ward: PropTypes.object,
    option: PropTypes.string,
    totalPrice: PropTypes.number,
    discount: PropTypes.number,
}

const mapStateToProps = (state) => ({
    token: state.authenticationReducer.accessToken,
    basketitem: state.merchantDetailReducer.basketitem,
    addr: state.addressReducer.addr,
    district: state.addressReducer.district,
    ward: state.addressReducer.ward,
    option: state.optionReducer.option,
    totalPrice: state.merchantDetailReducer.totalPrice,
    discount: state.merchantDetailReducer.discount,
})

export default connect(mapStateToProps)(Basket);