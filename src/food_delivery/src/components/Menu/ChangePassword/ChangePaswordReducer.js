import { CHANGE_REQUEST, CHANGE_SUCCESS, CHANGE_FAIL } from './ChangePasswordAction';
const initialState = {
    updating: false,
}
const changePasswordReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_REQUEST:
            return { ...state, updating: true };
        case CHANGE_SUCCESS:
            return { ...state, updating: false };
        case CHANGE_FAIL:
            return { ...state, updating: false };
        default:
            return state;
    }
}

export default changePasswordReducer;