import axios from 'axios';
export const getNearMerchant = async (lat, long) => {
    const json = await axios.get('https://food-delivery-server.herokuapp.com/restaurant/nearMe/' + lat + '&' + long);
    return await json.data;
}