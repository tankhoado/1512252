import React, { Component } from 'react';
import {
  ScrollView,
  Text,
} from 'react-native';
import styles from '../../../style';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { signOut } from '../Authentication/AuthenticationAction';
import { strings } from '../../../localization/i18n';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
class Menu extends Component {
  render() {
    const { signOut } = this.props;
    return (
      <ScrollView scrollsToTop={false} style={styles.menuBar} >
        <Text
          onPress={() => Actions.updateInfo({ title: strings('updateInfo') })}
          style={styles.menuItem}
        >
          <Icon name='information' size={26} /> {strings('updateInfo')}
        </Text>
        <Text
          onPress={() => Actions.changePass({ title: strings('changePass') })}
          style={styles.menuItem}
        >
          <Icon name='lock-reset' size={26} /> {strings('changePass')}
        </Text>
        <Text
          onPress={() => Actions.history({ title: strings('history') })}
          style={styles.menuItem}
        >
          <Icon name='history' size={26} /> {strings('history')}
        </Text>
        <Text
          onPress={() => signOut()}
          style={styles.menuItem}
        >
          <Icon name='logout' size={26} /> {strings('signOut')}
        </Text>
      </ScrollView>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  signOut: () => {
    dispatch(signOut());
  }
})

export default connect(undefined, mapDispatchToProps)(Menu);
