import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist'
import authenticationReducer from '../components/Authentication/AuthenticationReducer';
import addressReducer from '../components/Dialog/Address/AddressReducer';
import historyOrderReducer from '../components/DeliveryOrder/HistoryOrder/HistoryOrderReducer';
import merchantReducer from '../components/Merchant/MerchantReducer';
import merchantDetailReducer from '../components/Merchant/MerchantDetail/MerchantDetailReducer';
import optionReducer from '../components/Dialog/Option/OptionReducer';
import updateInfoReducer from '../components/Menu/UpdateInfo/UpdateInfoReducer';
import changePasswordReducer from '../components/Menu/ChangePassword/ChangePaswordReducer';
import promoReducer from '../components/Dialog/Promo/PromoReducer';
import storage from 'redux-persist/lib/storage';

const authenticationConfig = {
    key: 'authentication',
    storage,
    blacklist: ['isAuthenticating', 'error']
}

const addressConfig = {
    key: 'address',
    storage,
    blacklist: ['loadingWard']
}

const updateInfoConfig = {
    key: 'updateInfo',
    storage,
    blacklist: ['updating'],
}

const rootReducer = combineReducers({
    merchantReducer,
    changePasswordReducer,
    updateInfoReducer: persistReducer(updateInfoConfig, updateInfoReducer),
    optionReducer,
    historyOrderReducer,
    merchantDetailReducer,
    authenticationReducer: persistReducer(authenticationConfig, authenticationReducer),
    addressReducer: persistReducer(addressConfig, addressReducer),
    promoReducer,
})

export default rootReducer;