/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Router, Scene, Tabs, Stack } from 'react-native-router-flux';
import Login from './src/components/Authentication/Login';
import ForgetPass from './src/components/Authentication/ForgetPass';
import Merchant from './src/components/Merchant';
import Register from './src/components/Authentication/Register';
import MerchantDetail from './src/components/Merchant/MerchantDetail';
import Basket from './src/components/Basket';
import DeliveryOrder from './src/components/DeliveryOrder';
import HistoryOrder from './src/components/DeliveryOrder/HistoryOrder';
import UpdateInfo from './src/components/Menu/UpdateInfo';
import Map from './src/components/Map';
import HistoryDetail from './src/components/DeliveryOrder/HistoryOrder/HistoryDetail';
import ChangePassword from './src/components/Menu/ChangePassword';
import Loading from './src/components/Authentication/Loading';
import { strings } from './localization/i18n';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import Icon from 'react-native-vector-icons/Feather';
import I18n from 'react-native-i18n';
class App extends Component {
  constructor(props) {
    super(props);
    const { switchOn } = this.props;
    I18n.locale = switchOn ? 'vi' : 'en';;
  }
  componentDidMount() {
    SplashScreen.hide();
  }
  switchLanguage(value) {
    I18n.locale = value ? 'vi' : 'en';;
  }
  render() {
    const mapIcon = props => (
      <Icon name='map' size={26} color='#1B67FF' />
    );
    const listIcon = props => (
      <Icon name='list' size={26} color='#1B67FF' />
    )
    return (
      <Router>
        <Scene modal key='root'>
          <Scene key='loading' component={Loading} hideNavBar />
          <Scene key='forgetpass' component={ForgetPass} />
          <Scene key='register' component={Register} />
          <Scene key='login' component={Login} title={strings('login.title')} renderLeftButton={() => (null)} />
          <Scene
            key='merchant'
            hideNavBar
            tabs={true}
            labelStyle={{ fontSize: 16 }}
            inactiveTintColor={'black'}
            tabBarStyle={{ backgroundColor: '#eee' }}>
            <Scene key={'tab1'} icon={listIcon} title={strings('list')}>
              <Scene key={'sub11'}
                component={Merchant}
                initial
                hideNavBar
              >
              </Scene>
            </Scene>
            <Scene key={'tab2'} icon={mapIcon} title={strings('map')} >
              <Scene key={'sub21'}
                component={Map}
                hideNavBar>
              </Scene>
            </Scene>
          </Scene>
          <Scene key='merchantDetail' component={MerchantDetail} back />
          <Scene key='basket' component={Basket} />
          <Scene key='deliveryOrder' component={DeliveryOrder} hideNavBar />
          <Scene key='history' component={HistoryOrder} />
          <Scene key='historyDetail' component={HistoryDetail} title={strings('detail')} />
          <Scene key='updateInfo' component={UpdateInfo} switchLanguage={value => this.switchLanguage(value)} />
          <Scene key='changePass' component={ChangePassword} />
        </Scene>
      </Router>
    );
  }
}
const mapStateToProps = (state) => ({
  switchOn: state.updateInfoReducer.switchOn,
})

export default connect(mapStateToProps)(App);