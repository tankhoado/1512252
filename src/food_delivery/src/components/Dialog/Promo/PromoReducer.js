import {
    UPDATE_PROMO
} from './PromoAction';

const initialState = {
    promo: ''
}

const promoReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_PROMO:
            return { ...state, promo: action.data };
        default:
            return state;
    }
}

export default promoReducer;