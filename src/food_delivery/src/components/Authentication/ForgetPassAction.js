import { Alert } from 'react-native';
import { strings } from '../../../localization/i18n';
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
export const forgetPass = (email) => {
    if (email == '') {
        Alert.alert(strings('error'), strings('emailEmpty'));
        return;
    }
    (async () => {
        try {
            const resp = await axios.post('https://food-delivery-server.herokuapp.com/forgetPassword', {
                email: email
            });
            if (resp.status === 200) {
                Alert.alert(strings('success'), resp.data.msg, [{
                    text: 'OK', onPress: () => {
                        Actions.pop();
                    }
                }]);
            }
        } catch (e) {
            console.log(e);
        }
    })();
}