import axios from 'axios';
import { Alert } from 'react-native';
import sha256 from 'crypto-js/sha256';
import { strings } from '../../../../localization/i18n';
import { signOut } from '../../Authentication/AuthenticationAction';
export const CHANGE_REQUEST = 'CHANGE_REQUEST'
export const CHANGE_SUCCESS = 'CHANGE_SUCCESS'
export const CHANGE_FAIL = 'CHANGE_FAIL'

export const changePassword = data => dispatch => {
    dispatch({
        type: CHANGE_REQUEST,
    })
    const hash = sha256(data.oldPass).toString();
    if (hash !== data.passHash) {
        Alert.alert(strings('error'), strings('oldPassWrong'));
        dispatch({
            type: CHANGE_FAIL,
        })
    }
    else {
        const hashNewPass = sha256(data.newPass).toString();
        if (hashNewPass === data.passHash) {
            Alert.alert(strings('error'), strings('matchOldPass'));
            dispatch({
                type: CHANGE_FAIL,
            })
        }
        else {
            axios.post('https://food-delivery-server.herokuapp.com/updatePassword', {
                password: data.newPass,
            }, {
                    headers: { "Authorization": 'Bearer ' + data.token }
                }).then(resp => {
                    dispatch({
                        type: CHANGE_SUCCESS,
                    });
                    Alert.alert(strings('success'), strings('changePasswordSuccess'), [
                        { text: 'OK', onPress: () => dispatch(signOut()) },
                    ]);
                }).catch(e => {
                    dispatch({
                        CHANGE_FAIL,
                    });
                    Alert.alert(strings('error'), e.response.status);
                });
        }
    }
}