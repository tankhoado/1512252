import axios from 'axios';
import sha256 from 'crypto-js/sha256';
import { Actions } from 'react-native-router-flux';
export const AUTHENTICATION_REQUEST = 'AUTHENTICATION_REQUEST';
export const AUTHENTICATION_SUCCESS = 'AUTHENTICATION_SUCCESS';
export const AUTHENTICATION_FAILURE = 'AUTHENTICATION_FAILURE';
export const SIGN_OUT = 'SIGN_OUT';

export const authenticate = (data) => (dispatch) => {
    dispatch({
        type: AUTHENTICATION_REQUEST,
    });
    axios.post('https://food-delivery-server.herokuapp.com/login', data).then(response => {
        dispatch({
            type: AUTHENTICATION_SUCCESS,
            message: response,
            passHash: sha256(data.password).toString(),
        })
    }).catch(e => {
        dispatch({
            type: AUTHENTICATION_FAILURE,
            message: e,
        })
    });
}

export const signOut = () => (dispatch) => {
    dispatch({
        type: SIGN_OUT,
    });
}